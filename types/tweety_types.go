// @Deprecated
package types

import (
	"strconv"
	"time"
)

type User struct {
	Id              uint64            `json:"id"`
	Id_str          string            `json:"id_str"`
	Name            string            `json:"name"`
	Screen_name     string            `json:"screen_name"`
	Location        string            `json:"location"`
	URL             string            `json:"url"`
	Description     string            `json:"description"`
	Protected       bool              `json:"protected"`
	Verified        bool              `json:"verified"`
	Followers_count uint64            `json:"followers_count"`
	Friends_count   uint64            `json:"friends_count"`
	Statuses_count  uint64            `json:"statuses_count"`
	Created_at      time.Time         `json:"created_at"`
	Followers_id    []string          `json:"list_of_follower_ids"`
	Ten_words       map[string]uint64 `json:"word_counts"`
	Last_modified   time.Time         `json:"last_modified"`
}

type TwitterApiUser struct {
	Id          uint64 `json:"id"`
	Id_str      string `json:"id_str"`
	Name        string `json:"name"`
	Screen_name string `json:"screen_name"`
	Location    string `json:"location"`
	URL         string `json:"url"`
	Entities    struct {
		Urls []struct {
			Url string `json:"expanded_url"`
		} `json:"urls"`
	} `json:"entities"`
	Description     string      `json:"description"`
	Protected       bool        `json:"protected"`
	Verified        bool        `json:"verified"`
	Followers_count uint64      `json:"followers_count"`
	Friends_count   uint64      `json:"friends_count"`
	Statuses_count  uint64      `json:"statuses_count"`
	Created_at      TwitterTime `json:"created_at"`
}

type Friends struct {
	Friends_ids []string `json:"ids"`
}

type Tweet struct {
	Tweet_id     uint64    `json:"tweet_id"`
	Tweet_id_str string    `json:"tweet_id_str"`
	User_id      uint64    `json:"user_id"`
	Created_at   time.Time `json:"created_at"`
	Text         string    `json:"text"`
	URL          string    `json:"URL"`
}

type TweetsForDB struct {
	UserId    uint64            `json:"user_id"`
	Tweets    []TwitterApiTweet `json:"tweets"`
	WordCount map[string]uint64 `json:"word_count"`
	AppName   string            `json:"app_name"`
	SentAt    time.Time         `json:"timestamp"`
}

type TwitterApiTweet struct {
	Created_at TwitterTime `json:"created_at"`
	Id         uint64      `json:"id"`
	Id_str     string      `json:"id_str"`
	Text       string      `json:"full_text"`
	Url        string
	User       struct {
		Id          uint64 `json:"id"`
		Screen_name string `json:"screen_name"`
	} `json:"user"`
}

type TwitterTime struct {
	time.Time
}

type UserId struct {
	User_id  string    `json:"user_id"`
	App_name string    `json:"app_name"`
	Sent_at  time.Time `json:"timestamp"`
}

type UserExistsResp struct {
	Exists        bool      `json:"exists"`
	Last_modified time.Time `json:"last_modified"`
}

type TweetId struct {
	Id string `json:"tweet_id"`
}

func (twTime *TwitterTime) UnmarshalJSON(b []byte) error {
	bString := string(b)
	bString, err := strconv.Unquote(bString)
	if err != nil {
		return err
	}
	myTime, err := time.Parse(time.RubyDate, bString)
	if err != nil {
		return err
	}
	*twTime = TwitterTime{myTime}

	return nil
}
